import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author gaudencio
 * Jul 6, 2011
 * VendingMachine
 */
public class Gaveta{

	ArrayList<Produto> p;
	int itens;

	/**
	 * A gaveta e um conjunto de produtos.
	 * 
	 * @param max
	 * - capacidade maxima da gaveta
	 */
	public Gaveta(int max){
		p = new ArrayList<Produto>();
		itens = max;
	}
	
	/**
	 * Preenche a gaveta com a quantidade de produtos pretendida.
	 * 
	 * @param prod
	 * - produto pretendido
	 * @param q
	 * - quantidade
	 */
	public void setSeleccao(Produto prod, int q){
		//enche a fila com esse produto ate ao maximo
		for(int i=0;i<q;i++)
			p.add(prod);
	}
	
	/**
	 * @return
	 * Produto.
	 */
	public Produto getProduto(){
		return p.get(itens-1);
	}
	
	/**
	 * Remove um produto da gaveta seleccionada.
	 */
	public void removeProduto(){
		p.remove(itens-1);
		this.itens--;
	}
	
	/**
	 * @return
	 * - quantidade de itens da gaveta
	 */
	public int getItens(){
		return this.itens;
	}

	public String toString(){
		StringBuffer descricao = new StringBuffer ("");
		Iterator<Produto> it = p.iterator();
		while(it.hasNext())	
			descricao.append(it.next().toString()+"\n");
		descricao.append("\n----");
		return descricao.toString();
	}
	
}