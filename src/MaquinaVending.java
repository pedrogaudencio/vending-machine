
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import p2.maquinavending.currency.*;

/**
 * 
 * @author gaudencio
 * Jul 6, 2011
 * VendingMachine
 *
 */
public class MaquinaVending{

	Gaveta[][] gavetas;
	boolean[][] booleanGaveta;
	int quantidade, prateleiras, seleccoes, linha, coluna = 1, menu;

	String s;
	char c1, c2;
	int s1, s2;

	double moedas = 0.0, quantia = 0.0, troco = 0.0;

	CaixinhaTrocos caixinha;
	int nrmoedasDois = 0, nrmoedasUm = 0, nrmoedasCinquenta = 0, nrmoedasVinte = 0, nrmoedasDez = 0;

	/**
	 * ArrayList de moedas temporário para guardar as moedas
	 * que o utilizador insere.
	 */
	ArrayList<Moeda> moedasTemp = new ArrayList<Moeda>();

	/**
	 * ArrayList de moedas temporário para guardar as moedas
	 * que irão fazer parte do troco.
	 */
	ArrayList<Moeda> trocoT = new ArrayList<Moeda>();
	
	/**
	 * ArrayList de produtospara guardar os produtos que já foram vendidos
	 */
	ArrayList<Produto> produtosVendidos = new ArrayList<Produto>();
	
	/**
	 * ArrayList de produtos para guardar os produtos que não foram vendidos e passaram o prazo de validade
	 */
	ArrayList<Produto> validadeVendidos = new ArrayList<Produto>();

	static DoisEuros mDois = new DoisEuros();
	static UmEuro mUm = new UmEuro();
	static CinquentaCentimos mCinquenta = new CinquentaCentimos();
	static VinteCentimos mVinte = new VinteCentimos();
	static DezCentimos mDez = new DezCentimos();

	/**
	 * Cria uma nova Vending Machine com as prateleiras, seleccoes e
	 * quantidade máxima pretendidas.
	 * 
	 * @param prateleiras
	 * - número de prateleiras da máquina (linhas)
	 * @param seleccoes
	 * - número de seleccoes pretendido
	 * @param maximo
	 * - quantidade máxima por gaveta (seleccão)
	 */
	public MaquinaVending(int prateleiras, int seleccoes, int maximo){
		this.prateleiras = prateleiras;
		this.seleccoes = seleccoes;
		gavetas = calculaTamanho(prateleiras, seleccoes);
		booleanGaveta = new boolean[linha][coluna];
		preencheArray(booleanGaveta, false, linha, coluna);
		for(int i=0; i<linha; i++){
			for(int j=0; j<coluna; j++){
				gavetas[i][j] = new Gaveta(maximo);
			}
		}
		this.quantidade = maximo;
		caixinha = new CaixinhaTrocos();
		preencheCaixinha();
	}

	/**
	 * Menu de opcoes
	 */
	public void menu(){
		while(true){
			System.out.println("Escolha uma das opcoes:\n1: Seleccionar produto\n2: Manutencão\n");
				try{
					Scanner sc = new Scanner(System.in);
					menu = sc.nextInt();
					switch(menu){				
						case 1:	visorProduto(); break;
						case 2: visorManutencao(); break;
						case 3: visorSair(); break;
						default: System.out.println("Insira uma opcão válida.\n") ; menu(); break;
					}
				} catch(java.util.InputMismatchException e){
					System.out.println("Opcão inválida.\n");
					break;
				}
			}
	}

	/**
	 * Pede a seleccão ao utilizador e converte-a para inteiros de modo
	 * a poder aceder ao array gaveta[][].
	 */
    public void visorProduto(){
    	try{
    		Scanner sc = new Scanner(System.in);
    		s = sc.next().toUpperCase();
			c1 = s.charAt(0);
	    	c2 = s.charAt(1);
    	} catch (java.lang.StringIndexOutOfBoundsException e){}
    	digitaSeleccao(c1,c2);
	}

    /**
     * Menu de opcoes de manutencão
     */
	public void visorManutencao(){
		while(true){
			System.out.println("Escolha uma das opcoes:" +
								"\n1: Carregar gaveta com produtos" +
								"\n2: Esvaziar gaveta" +
								"\n3: Carregar a caixinha de trocos" +
								"\n4: Produtos vendidos" +
								"\n5: Produtos não vendidos fora do prazo" +
								"\n6: Produtos mais vendidos" +
								"\n7: Dinheiro realizado" +
								"\n8: Retroceder\n");
				try{
					Scanner sc = new Scanner(System.in);
					int menu = sc.nextInt();
					switch(menu){				
						case 1:	carregaGaveta(); break;
						case 2:	esvaziaGaveta(); break;
						case 3: preencheCaixinha(); break;
						case 4: vendidosProdutosListar(); break;
						case 5: vendidosValidade(); break;
						case 6: maisVendido(); break;
						case 7: dinheiroRealizado(); break;
						case 8: menu(); break;
						default: System.out.println("Insira uma opcão válida.\n") ; menu(); break;
					}
				} catch(java.util.InputMismatchException e){
					System.out.println("Opcão inválida.\n");
					break;
				}
			}
	}

	/**
	 * Termina a máquina e sai do programa
	 */
	public void visorSair(){
		System.exit(0);		
	}

	/**
	 * Calcula o tamanho da máquina (linhas e colunas) recebendo o
	 * número de prateleiras e seleccoes. Enquanto o número de prateleiras
	 * for menor que o número de seleccoes aumenta uma coluna na máquina, de
	 * maneira a satisfazer as seleccoes pedidas e a gerar uma máquina com um
	 * tamanho mais homogeneo.
	 * 
	 * @param prateleiras
	 * - número de prateleiras da máquina (linhas)
	 * @param seleccoes
	 * - número de seleccoes pretendido
	 * @return
	 * Array do tipo Gaveta com o tamanho de linhas e
	 * colunas geradas anteriormente.
	 */
	public Gaveta[][] calculaTamanho(int prateleiras, int seleccoes){
		if(prateleiras<seleccoes){
			prateleiras+=prateleiras;
			coluna++;
			calculaTamanho(prateleiras, seleccoes);
		} else {
			linha=prateleiras/coluna;
			prateleiras=linha;
		}
		return new Gaveta[linha][coluna];
	}

	/**
	 * Preenche o array que recebe como argumento com o boolean que recebe como argumento.
	 * 
	 * @param b
	 * - array de booleans que vai ser preenchido
	 * @param boo
	 * - boolean que vai preencher o array
	 * @param linha
	 * - tamanho das linhas a serem preenchidas
	 * @param coluna
	 * - tamanho das colunas a serem preenchidas
	 */
	public void preencheArray(boolean[][] b, boolean boo, int linha, int coluna){
		for(int i=0;i<linha;i++){
			for(int j=0;j<coluna;j++){
				b[i][j]=boo;
			}
		}
	}

	/**
	 * Preenche a respectiva gaveta com o produto pretendido e marca a posicão no array booleanGaveta como true.
	 *   
	 * @param m
	 * - posicão na linha a ser preenchida
	 * @param n
	 * - posicão na coluna a ser preenchida
	 * @param prod
	 * - produto que vai preencher a posicão
	 */
	public void adicionaProduto(int m, int n, Produto prod) {
		gavetas[m][n].setSeleccao(prod, quantidade);
		booleanGaveta[m][n]=true;
	}

	/**
	 * @return
	 * Número de linhas.
	 */
	public int getLinhas(){
		return this.linha;
	}

	/**
	 * @return
	 * Número de colunas.
	 */
	public int getColunas(){
		return this.coluna;
	}

	/**
	 * @return
	 * Quantidade de itens na gaveta.
	 */
	public int getQuantidade() {
		return quantidade;
	}

	/**
	 * @param quantidade
	 * - quantidade de itens na gaveta
	 */
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	/**
	 * Enche a caixinha de trocos com moedas. 
	 */
	public void preencheCaixinha(){
		caixinha.insereQuantidade(mDois, 5);
		caixinha.insereQuantidade(mUm, 10);
		caixinha.insereQuantidade(mCinquenta, 20);
		caixinha.insereQuantidade(mVinte, 50);
		caixinha.insereQuantidade(mDez, 100);
	}

	/**
	 * Digita a seleccão no teclado da máquina.
	 * 
	 * @param linha
	 * - argumento da linha
	 * @param coluna
	 * - argumento da coluna
	 */
	public void digitaSeleccao(char linha, char coluna){
	
		s1 = (char) linha-65;
		s2 = (char) coluna-65;
		verificaProduto(s1, s2);
	}

	/**
	 * Verifica se na seleccão pretendida existem produtos e se estão dentro 
	 * do prazo de validade. Caso não reunam as condicoes apresenta a mensagem 
	 * "Produto inexistente".
	 * 
	 * @param linha
	 * - linha da seleccao
	 * @param coluna
	 * - coluna da seleccao
	 */
	public void verificaProduto(int linha, int coluna){
			try {
			if(booleanGaveta[linha][coluna]==true && gavetas[linha][coluna].getProduto().verificaValidade()==true)
				introduzDinheiro(gavetas[linha][coluna].getProduto());
			else
				System.out.println("Produto inexistente.\n");
			} catch (java.lang.ArrayIndexOutOfBoundsException e) {
					System.out.println("Insira uma seleccao válida.\n");
				}
		}

	/**
	 * Pede ao utilizador a quantia necessária para efectuar a compra. Enquanto
	 * nao for suficiente continua a pedir - apresentando o valor do produto - e
	 * vai guardando as moedas numa ArrayList temporária e somando o valor destas
	 * atE' ser maior ou igual ao valor do produto.
	 * 
	 * @param p
	 * - produto pretendido previamente seleccionado
	 */
	public void introduzDinheiro(Produto p){
		while(true){
			try{
				Scanner sc = new Scanner(System.in);
				System.out.println("Insira a quantia ou anule a compra no botao [anular]");
					moedas = sc.nextDouble();
					moedas = arredondaDecimal(moedas);
					if(moedas==0.10){
						DezCentimos tempDez = new DezCentimos();
						moedasTemp.add(tempDez);
					} else if(moedas==0.20){
						VinteCentimos tempVinte = new VinteCentimos();
						moedasTemp.add(tempVinte);
					} else if(moedas==0.50){
						CinquentaCentimos tempCinquenta = new CinquentaCentimos();
						moedasTemp.add(tempCinquenta);
					} else if(moedas==1.0){
						UmEuro tempUm = new UmEuro();
						moedasTemp.add(tempUm);
					} else if(moedas==2.0){
						DoisEuros tempDois = new DoisEuros();
						moedasTemp.add(tempDois);
					} else {
						System.out.println("Moeda inválida.\n" + p.getPreco());
						continue;}
			} catch(java.util.InputMismatchException e){
				Iterator<Moeda> it = moedasTemp.iterator();
				StringBuffer descricao = new StringBuffer ("Compra anulada [x]\nDevolucao de moedas:\n");
				while(it.hasNext())
					descricao.append(it.next().toString()+"\n");
				System.out.println(descricao);
				moedasTemp.clear();
				quantia=0.0;
				menu();
				continue;
			}
			quantia+=moedas;
			if(arredondaDecimal(quantia)>=p.getPreco())
				break;
			else{
				System.out.println(p.getPreco());
				continue;
			}
		} //fim do while
		troco = arredondaDecimal(quantia) - p.getPreco();
		troco = arredondaDecimal(troco);
		if(troco!=0)
			calculaTroco(arredondaDecimal(troco));
		else{
			quantia=0.0;
			troco=0.0;
			nrmoedasDois = 0;
			nrmoedasUm = 0;
			nrmoedasCinquenta = 0;
			nrmoedasVinte = 0;
			nrmoedasDez = 0;
			guardaMoedas(moedasTemp);
			retiraProduto(s1, s2);
		}
	}

	/**
	 * Arredonda doubles atE' às casas dE'cimais
	 * @param d
	 * - double a ser arredondado
	 * @return
	 * Double arredondado.
	 */
	public double arredondaDecimal(double d){
		double dd = d*100;
		dd = Math.round(dd);
		dd/=100;
		return dd;
	}

	/**
	 * Verifica se o troco E' maior ou igual ao valor dos diferentes
	 * tipos de moedas, recursivamente. Vai decrementando o troco e
	 * guardando as moedas num array de moedas para depois verificar
	 * se existe troco suficiente; caso nao exista E' mostrada a mensagem
	 * "Quantia correcta" e o utilizador terá de introduzir a quantia
	 * correcta para poder efectuar a compra.
	 * 
	 * @param trocoTemp
	 * - quantia de troco a devolver
	 */
	public void calculaTroco(double trocoTemp){
			if(trocoTemp>=2){
				if(nrmoedasDois>caixinha.doisEuros.count()){
					nrmoedasUm+=nrmoedasDois*2;
					trocoTemp-=2;
					calculaTroco(arredondaDecimal(trocoTemp));
				} else if(nrmoedasDois<=caixinha.doisEuros.count()){
					nrmoedasDois++;
					trocoTemp-=2;
					calculaTroco(arredondaDecimal(trocoTemp));
				}
			} else if(trocoTemp>=1){
				if(nrmoedasUm>caixinha.umEuro.count()){
					nrmoedasCinquenta+=nrmoedasUm*2;
					trocoTemp-=1;
					calculaTroco(arredondaDecimal(trocoTemp));
				} else if(nrmoedasUm<=caixinha.umEuro.count()){
					nrmoedasUm++;
					trocoTemp-=1;
					calculaTroco(arredondaDecimal(trocoTemp));
				}
			} else if(trocoTemp>=0.5){
				if(nrmoedasCinquenta>caixinha.cinquentaCentimos.count()){
					nrmoedasVinte+=nrmoedasCinquenta*2;
					nrmoedasDez+=nrmoedasCinquenta;
					trocoTemp-=0.5;
					calculaTroco(arredondaDecimal(trocoTemp));
				} else if(nrmoedasCinquenta<=caixinha.cinquentaCentimos.count()){
					nrmoedasCinquenta++;
					trocoTemp-=0.5;
					calculaTroco(arredondaDecimal(trocoTemp));
				}
			} else if(trocoTemp>=0.2){
				if(nrmoedasVinte>caixinha.vinteCentimos.count()){
					nrmoedasDez+=nrmoedasVinte*2;
					trocoTemp-=0.2;
					calculaTroco(arredondaDecimal(trocoTemp));
				} else if(nrmoedasVinte<=caixinha.vinteCentimos.count()){
					nrmoedasVinte++;
					trocoTemp-=0.2;
					calculaTroco(arredondaDecimal(trocoTemp));
				}
			} else if(trocoTemp>=0.1 && nrmoedasDez>caixinha.dezCentimos.count()){
					Iterator<Moeda> it = moedasTemp.iterator();
					StringBuffer descricao = new StringBuffer ("Devolucao de moedas:\n");
					while(it.hasNext())
						descricao.append(it.next().toString()+"\n");
					System.out.println(descricao);
					moedasTemp.clear();
					quantia=0.0;
					quantiaCorrecta();
			} else if(trocoTemp>=0.1 && nrmoedasDez>caixinha.dezCentimos.count()){
					nrmoedasDez++;
					trocoTemp-=0.1;
					calculaTroco(arredondaDecimal(trocoTemp));
			}
			while(troco!=0){
				if(troco>=2){
					trocoT.add(mDois);
					troco-=2;
					troco = arredondaDecimal(troco);
					//nrmoedasDois++;
					calculaTroco(arredondaDecimal(troco));
				} else if(troco>=1){
					trocoT.add(mUm);
					troco-=1;
					troco = arredondaDecimal(troco);
					//nrmoedasUm++;
					calculaTroco(arredondaDecimal(troco));
				} else if(troco>=0.5){
					trocoT.add(mCinquenta);
					troco-=0.5;
					troco = arredondaDecimal(troco);
					//nrmoedasCinquenta++;
					calculaTroco(arredondaDecimal(troco));
				} else if(troco>=0.2){
					trocoT.add(mVinte);
					troco-=0.2;
					troco = arredondaDecimal(troco);
					//nrmoedasVinte++;
					calculaTroco(arredondaDecimal(troco));
				} else if(troco>=0.1){
					trocoT.add(mDez);
					troco-=0.1;
					troco = arredondaDecimal(troco);
					//nrmoedasDez++;
					calculaTroco(arredondaDecimal(troco));
				}
			}
			Iterator<Moeda> it = trocoT.iterator();
			StringBuffer descricao = new StringBuffer ("Troco:\n");
			while(it.hasNext())
				descricao.append(it.next().toString()+"\n");
			System.out.println(descricao);
			quantia=0.0;
			nrmoedasDois = 0;
			nrmoedasUm = 0;
			nrmoedasCinquenta = 0;
			nrmoedasVinte = 0;
			nrmoedasDez = 0;
			guardaMoedas(moedasTemp);
			retiraMoedas(trocoT);
			retiraProduto(s1, s2);
	}

	/**
	 * Pede a quantia correcta ao utilizador.
	 */
	public void quantiaCorrecta(){
		try{
			while(true){
				Scanner sc = new Scanner(System.in);
				System.out.println("Quantia correcta.");
					moedas = sc.nextDouble();
					moedas = arredondaDecimal(moedas);
					if(quantia==gavetas[s1][s2].getProduto().getPreco()){
						quantia=0.0;
						moedas=0.0;
						nrmoedasDois = 0;
						nrmoedasUm = 0;
						nrmoedasCinquenta = 0;
						nrmoedasVinte = 0;
						nrmoedasDez = 0;
						guardaMoedas(moedasTemp);
						retiraProduto(s1, s2);
					} else{
						if(moedas==2){
							moedasTemp.add(mDois);
							quantia+=2;
							quantiaCorrecta();
						} else if(moedas==1){
							moedasTemp.add(mUm);
							quantia+=1;
							quantiaCorrecta();
						} else if(moedas==0.5){
							moedasTemp.add(mCinquenta);
							quantia+=0.5;
							quantiaCorrecta();
						} else if(moedas==0.2){
							moedasTemp.add(mVinte);
							quantia+=0.2;
							quantiaCorrecta();
						} else if(moedas==0.1){
							moedasTemp.add(mDez);
							quantia+=0.1;
							quantiaCorrecta();
						}
					}
			}
		} catch(java.util.InputMismatchException e){
				System.out.println("Quantia inválida.");
				quantiaCorrecta();
			}
		}
	
	/**
	 * Guarda as moedas na caixinha de moedas.
	 * 
	 * @param m
	 * - ArrayList<Moeda> que contE'm as moedas que vao ser inseridas
	 * na caixinha 
	 */
	public void guardaMoedas(ArrayList<Moeda> m){
		for(int i=0;i<m.size();i++){
			caixinha.insereMoedas(m.get(i));
		}
		m.clear();
	}

	/**
	 * Retira as moedas da caixinha de moedas.
	 * 
	 * @param m
	 * - ArrayList<Moeda> que contE'm as moedas que vao ser retiradas
	 * da caixinha
	 */
	public void retiraMoedas(ArrayList<Moeda> m){
		for(int i=0;i<m.size();i++){
			caixinha.removeMoedas(m.get(i));
		}
		m.clear();
	}

	/**
	 * Retira o produto seleccionado da gaveta.
	 * 
	 * @param linha
	 * - prateleira que contE'm o produto seleccionado
	 * @param coluna
	 * - posicao da prateleira que contE'm o produto seleccionado
	 */
	public void retiraProduto(int linha, int coluna){
		System.out.println("Produto retirado:\n" + gavetas[linha][coluna].getProduto().toString() + "\n");
		produtosVendidos.add(gavetas[linha][coluna].getProduto());
		gavetas[linha][coluna].removeProduto();
		if(gavetas[linha][coluna].getItens()==0)
			booleanGaveta[linha][coluna]=false;
		menu();
	}

	/**
	 * Enche a gaveta seleccionada com produtos.
	 */
	public void carregaGaveta(){
		while(true){
			try{
				Scanner sc = new Scanner(System.in);
	    		s = sc.next().toUpperCase();
				c1 = s.charAt(0);
		    	c2 = s.charAt(1);
			} catch (java.lang.StringIndexOutOfBoundsException e){
				System.out.println("Seleccao inválida.");
			}
			try{
		    	digitaGaveta(c1,c2);
		    	gavetas[s1][s2].setSeleccao(gavetas[s1][s2].getProduto(), quantidade-gavetas[s1][s2].getItens());
				booleanGaveta[s1][s2]=true;
			}
			catch (java.lang.ArrayIndexOutOfBoundsException e){}
			visorManutencao();
		}
	}
	
	/**
	 * Digita a seleccao no teclado da máquina.
	 * 
	 * @param linha
	 * - argumento da linha
	 * @param coluna
	 * - argumento da coluna
	 */
	public void digitaGaveta(char linha, char coluna){
		s1 = (char) linha-65;
		s2 = (char) coluna-65;
	}
	
	/**
	 * Esvazia os produtos da gaveta seleccionada.
	 */
	public void esvaziaGaveta(){
		while(true){
			try{
				Scanner sc = new Scanner(System.in);
	    		s = sc.next().toUpperCase();
				c1 = s.charAt(0);
		    	c2 = s.charAt(1);
		    	break;
			} catch (java.lang.StringIndexOutOfBoundsException e){
				System.out.println("Seleccao inválida.");
			}
		}
		try{
	    	digitaGaveta(c1,c2);
			gavetas[s1][s2].p.clear();
		} catch (java.lang.ArrayIndexOutOfBoundsException e){
			System.out.println("Seleccao invalida.");
		}
		visorManutencao();
	}
	
	/**
	 * Lista os produtos que ja foram vendidos
	 */
	public void vendidosProdutosListar(){
		if(produtosVendidos.isEmpty())
			System.out.println("Nao foram vendidos produtos.");
		else{
			StringBuffer descricao = new StringBuffer ("Produtos vendidos:\n");
			Iterator<Produto> it = produtosVendidos.iterator();
			while(it.hasNext())
				descricao.append(it.next().toString()+"\n");
			System.out.println(descricao);
			}
		visorManutencao();
	}
	
	/**
	 * Lista os produtos mais vendidos.
	 */
	public void maisVendido(){
		Map<Produto,Integer> map = new HashMap<Produto, Integer>();
		for(int i=0;i<produtosVendidos.size();i++){
			Integer count = map.get(produtosVendidos.get(i));         
            map.put(produtosVendidos.get(i), count==null?1:count+1);
		}
		System.out.println(map);
	}
		
	
	/**
	 * Lista os produtos que nao foram vendidos e passaram do prazo de validade.
	 */
	public void vendidosValidade(){
		for(int i=0;i<linha;i++){ 
			for(int j=0;j<coluna;j++){
				for(int k=0;k<gavetas[i][j].getItens();k++){
					if(!gavetas[i][j].getProduto().verificaValidade())
						validadeVendidos.add(gavetas[i][j].getProduto());
				}
			}
		}
		if(validadeVendidos.isEmpty())
			System.out.println("Nao existem produtos fora do prazo de validade.");
		else{
			StringBuffer descricao = new StringBuffer ("Produtos nao vendidos fora do prazo de validade:\n");
			Iterator<Produto> it = validadeVendidos.iterator();
			while(it.hasNext())	
				descricao.append(it.next().toString()+"\n");
			System.out.println(descricao);
			}
		visorManutencao();
	}
	
	
	/**
	 * Mostra quanto dinheiro ja foi realizado.
	 */
	public void dinheiroRealizado(){
		if(produtosVendidos.isEmpty())
			System.out.println("Nao foram vendidos produtos.");
		else{
			double dinheiro = 0.0;
			for(int i=0;i<produtosVendidos.size();i++){
				dinheiro+=produtosVendidos.get(i).getPreco();
				arredondaDecimal(dinheiro);
			}
			System.out.println("Total de dinheiro realizado: " + dinheiro + "€");
		}
		visorManutencao();
	}
	
	
	/**
	 * E' tipo um toString() da MaquinaVending para imprimir os
	 * produtos que estao nas prateleiras.
	 */
	public void debug(){
		System.out.println();
		for(int i=0; i<linha; i++){
			for(int j=0; j<coluna; j++){
				System.out.println("Itens: "+ Integer.toString(quantidade) +
						 			" " + booleanGaveta[i][j] +
									"\nGaveta[" + i + "][" + j + "]\n" + 
										gavetas[i][j].toString());
			}
		}
		
	}

}