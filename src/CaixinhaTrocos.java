import p2.maquinavending.currency.*;
import p2.maquinavending.datastructure.*;

/**
 * @author gaudencio
 * Jul 7, 2011
 * VendingMachine
 */
public class CaixinhaTrocos{
	
	Fila doisEuros, umEuro, cinquentaCentimos, vinteCentimos, dezCentimos;
	
	DoisEuros mDois = new DoisEuros();
	UmEuro mUm = new UmEuro();
	CinquentaCentimos mCinquenta = new CinquentaCentimos();
	VinteCentimos mVinte = new VinteCentimos();
	DezCentimos mDez = new DezCentimos();
	
	/**
	 * Cria uma caixinha de trocos para a máquina
	 * constituída por 5 filas de diferentes tipos de moedas.
	 */
	public CaixinhaTrocos(){
		doisEuros = new Fila(10);
		umEuro = new Fila(20);
		cinquentaCentimos = new Fila(40);
		vinteCentimos = new Fila(100);
		dezCentimos = new Fila(200);
	}

	/**
	 * Insere moedas nas filas, separado-as consoante o tipo de moeda.
	 * @param m
	 * - moeda a ser inserida
	 */
	public void insereMoedas(Moeda m){
		try {
			if(m instanceof DezCentimos)
				dezCentimos.insert(mDez);
			else if(m instanceof VinteCentimos)
				vinteCentimos.insert(mVinte);
			else if(m instanceof CinquentaCentimos)
				cinquentaCentimos.insert(mCinquenta);
			else if(m instanceof UmEuro)
				umEuro.insert(mUm);
			else if(m instanceof DoisEuros)
				doisEuros.insert(mDois);
			} catch (MeNoWantYourMoneyException e) {
				e.getMessage();
			}
	}

	/**
	 * Insere moedas nas filas, separado-as consoante o tipo de moeda
	 * e a quantidade desejada.
	 * @param m
	 * - moeda a ser inserida
	 * @param q
	 * - quantidade de moedas a serem inseridas
	 */
	public void insereQuantidade(Moeda m, int q){
		try {
			for(int i=0;i<q;i++){
				if(m instanceof DezCentimos)
					dezCentimos.insert(mDez);
				else if(m instanceof VinteCentimos)
					vinteCentimos.insert(mVinte);
				else if(m instanceof CinquentaCentimos)
					cinquentaCentimos.insert(mCinquenta);
				else if(m instanceof UmEuro)
					umEuro.insert(mUm);
				else if(m instanceof DoisEuros)
					doisEuros.insert(mDois);
			}
			} catch (MeNoWantYourMoneyException e) {
				e.getMessage();
			}
	}

	/**
	 * Remove moedas das respectivas filas, tendo em atencao o tipo de moeda
	 * @param m
	 * - moeda que servira para comparar com o tipo de 
	 * fila donde se vai remover uma moeda 
	 */
	public void removeMoedas(Moeda m){
		try {
			if(m instanceof DezCentimos)
				dezCentimos.remove();
			else if(m instanceof VinteCentimos)
				vinteCentimos.remove();
			else if(m instanceof CinquentaCentimos)
				cinquentaCentimos.remove();
			else if(m instanceof UmEuro)
				umEuro.remove();
			else if(m instanceof DoisEuros)
				doisEuros.remove();
			} catch (MeNoHaveNoMoneyException e) {
				e.getMessage();
			}
	}
	
	
}