import java.util.GregorianCalendar;
import java.util.Date;
import java.text.DateFormat;

/**
 * @author gaudencio
 * Jul 6, 2011
 * VendingMachine
 */
public class Produto{
	
	String nome;
	String tipo;
	double preco;
	GregorianCalendar calendar = new GregorianCalendar();
	Date now = calendar.getTime();
	Date validade;
	
	/**
	 * Define um produto.
	 *  
	 * @param nome
	 * - nome do produto
	 * @param tipo
	 * - tipo do produto
	 * @param preco
	 * - preço do produto
	 * @param validade
	 * - data de validade do produto
	 */
	public Produto(String nome, String tipo, double preco, Date validade){
		this.nome = nome;
		this.tipo = tipo;
		this.preco = preco;
		this.validade = validade;
	}
	
	public String toString(){
		String descricao = new String("");
		descricao += "Nome: " + nome + ", tipo: "+ tipo + 
					", preço: "+ Double.toString(preco) + "€, validade: " + 
					DateFormat.getDateInstance().format(validade);
		return descricao;
	}
	
	/**
	 * @return
	 * - tipo do produto
	 */
	public String getTipo(){
		return this.tipo;
	}
	
	/**
	 * Verifica se o produto está dentro do prazo de validade. 
	 * @return
	 * - true se o prazo de validade está definido para depois da data actual.
	 */
	public boolean verificaValidade(){
		return validade.after(now);
	}
	
	/**
	 * @return
	 * - preço do produto.
	 */
	public Double getPreco(){
		return preco;
	}
	
}