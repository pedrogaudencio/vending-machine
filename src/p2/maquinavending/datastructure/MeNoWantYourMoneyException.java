package p2.maquinavending.datastructure;

@SuppressWarnings("serial")
public class MeNoWantYourMoneyException extends Exception{
	public MeNoWantYourMoneyException(String msg){
		super(msg);
	}
}