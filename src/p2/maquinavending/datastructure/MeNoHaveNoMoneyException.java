package p2.maquinavending.datastructure;

@SuppressWarnings("serial")
public class MeNoHaveNoMoneyException extends Exception{
	public MeNoHaveNoMoneyException(String msg){
		super(msg);
	}
}