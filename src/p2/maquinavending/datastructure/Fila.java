package p2.maquinavending.datastructure;
import p2.maquinavending.currency.*;

/**
 * @author gaudencio
 * Jul 7, 2011
 * VendingMachine
 */
public class Fila{
		
	int cabeca;
	int cauda;
	int tamanho;
	Moeda[] moedas;
	
	/**
	 * Constroi uma fila circular do tamanho desejado.
	 * @param tamanho
	 * - tamanho da fila
	 */
	public Fila(int tamanho){
		this.tamanho = tamanho;
		moedas = new Moeda[tamanho];
		cabeca = cauda = 0;
	}
	
	/**
	 * Verifica se a fila esta vazia.
	 * @return
	 * - true se esta vazia
	 */
	public boolean isEmpty(){
		return(cabeca==cauda);
	}
	
	/**
	 * Verifica se a fila esta cheia.
	 * @return
	 * - true se esta cheia
	 */
	public boolean isFull(){
		if(cabeca==cauda-1 || (cauda==tamanho-1 && cabeca==0))
			return true;
		return false;
	}
	
	/**
	 * Insere moedas na fila.
	 * @param m
	 * - moeda a ser inserida
	 * @throws MeNoWantYourMoneyException
	 * - se a fila esta cheia, lanca a excepcao
	 */
	public void insert(Moeda m) throws MeNoWantYourMoneyException{
		if(this.isFull()) throw new MeNoWantYourMoneyException("Está cheio.");
		else{
			moedas[cabeca]=m;
			cabeca++;
			cabeca=cabeca%tamanho;
		}
	}
	
	/**
	 * Remove moedas da fila.
	 * @throws MeNoHaveNoMoneyException
	 * - se a fila esta vazia, lanca a excepcao
	 */
	public void remove() throws MeNoHaveNoMoneyException{
		if(this.isEmpty()) throw new MeNoHaveNoMoneyException("Quantia correcta.");
		else{
			cauda--;
			cauda=cauda%tamanho;
		}
	}
	
	/**
	 * Conta quantas moedas estao na fila.
	 * @return
	 * - numero de moedas
	 */
	public int count(){
		if(isEmpty())
			return 0;
		else if(isFull())
			return tamanho;
		else if(cabeca<cauda)
			return (tamanho-cauda)+cabeca;
		else
			return cabeca-cauda;
	}
	
}