package p2.maquinavending.currency;
public class DezCentimos extends Moeda{
	
	/**
	 * Moeda de 0.10€
	 */
	public DezCentimos(){
		super.diametro = 19.75;
		super.peso = 4.10;
		super.valor = 0.10;
	}
	
}