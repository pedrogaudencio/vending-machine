package p2.maquinavending.currency;
public class DoisEuros extends Moeda{
	
	/**
	 * Moeda de 2€
	 */
	public DoisEuros(){
		super.diametro=25.75;
		super.peso=8.50;
		super.valor=2.0;
	}
	
}