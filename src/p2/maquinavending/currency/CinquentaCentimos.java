package p2.maquinavending.currency;
public class CinquentaCentimos extends Moeda{
	
	/**
	 * Moeda de 0.50€
	 */
	public CinquentaCentimos(){
		super.diametro = 24.25;
		super.peso = 7.80;
		super.valor = 0.50;
	}
	
}