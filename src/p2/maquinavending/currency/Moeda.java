package p2.maquinavending.currency;

/**
 * @author gaudencio
 * Jul 7, 2011
 * VendingMachine
 */
public class Moeda{

	double peso, diametro;
	double valor;

	public Moeda(){}
	
	/**
	 * @return
	 * - valor da moeda.
	 */
	public double getValor(){
		return this.valor;
	}
	
	/**
	 * @return
	 * - peso da moeda.
	 */
	public double getPeso(){
		return this.peso;
	}
	
	/**
	 * @return
	 * - diametro da moeda.
	 */
	public double getDiametro(){
		return this.diametro;
	}
	
	public String toString(){
		return new String("[valor: " + valor + "€ | diametro: " + diametro + "mm | peso: " + peso + "g]");
	}
		
}