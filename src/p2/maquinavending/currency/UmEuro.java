package p2.maquinavending.currency;
public class UmEuro extends Moeda{
	
	/**
	 * Moeda de 1€
	 */
	public UmEuro(){
		super.diametro = 23.25;
		super.peso = 7.5;
		super.valor = 1.0;
	}
	
}