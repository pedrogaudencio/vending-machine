package p2.maquinavending.currency;
public class VinteCentimos extends Moeda{
	
	/**
	 * Moeda de 0.20€
	 */
	public VinteCentimos(){
		super.diametro = 22.25;
		super.peso = 5.74;
		super.valor = 0.20;
	}
	
}