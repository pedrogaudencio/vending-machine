import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

/**
 * @author gaudencio
 * Jul 6, 2011
 * VendingMachine
 */
public class TestaMaquina{
	
	static MaquinaVending maquinaVending;
	
	static GregorianCalendar calendar = new GregorianCalendar();
	//calendar.add(GregorianCalendar.DATE, 5);
	static Date validadeMaisCinco = adicionaValidade(calendar, 5);
	static Date validadeMenosDez = adicionaValidade(calendar, -10);
	static Date validadeSempre = adicionaValidade(calendar, 400);
	
	static Produto Agua1 = new Produto("Água do Luso", "Águas", 0.50, validadeSempre);
	static Produto Agua2 = new Produto("Água Amanhecer", "Águas", 0.50, validadeSempre);
	static Produto Agua3 = new Produto("Água Entardecer", "Águas", 0.50, validadeSempre);
	static Produto Agua4 = new Produto("Água Anoitecer", "Águas", 0.50, validadeSempre);
	static Produto Agua5 = new Produto("Água da Chuva", "Águas", 0.50, validadeSempre);
	static Produto[] aguas = {Agua1, Agua2, Agua3, Agua4, Agua5};
	
	static Produto Cola = new Produto("Coca-Cola", "Refrigerantes", 1.0, validadeMaisCinco);
	static Produto Pepsi = new Produto("Pepsi", "Refrigerantes", 1.0, validadeMenosDez);
	static Produto SevenUp = new Produto("Seven Up", "Refrigerantes", 1.0, validadeMaisCinco);
	static Produto IceTea = new Produto("Ice Tea", "Refrigerantes", 1.0, validadeMenosDez);
	static Produto UmBongo = new Produto("Um Bongo", "Refrigerantes", 0.80, validadeMenosDez);
	static Produto[] refrigerantes = {Cola, Pepsi, SevenUp, IceTea, UmBongo};
	
	static Produto Batatas = new Produto("Batatas Fritas Lays", "Snacks", 0.80, validadeMaisCinco);
	static Produto KelloggsCereais = new Produto("Kelloggs Cereais", "Snacks", 0.60, validadeMenosDez);
	static Produto SandesFiambre = new Produto("Sandes de Fiambre", "Snacks", 0.80, validadeMenosDez);
	static Produto SandesQueijo = new Produto("Sandes de Queijo", "Snacks", 0.80, validadeMaisCinco);
	static Produto SandesMista = new Produto("Sandes Mista", "Snacks", 0.80, validadeMaisCinco);
	static Produto[] snacks = {Batatas, KelloggsCereais, SandesFiambre, SandesQueijo, SandesMista};
	
	static Produto Sugus = new Produto("Sugus", "Goluseimas", 1.0, validadeMaisCinco);
	static Produto PastilhasMenta = new Produto("Pastilhas de Menta", "Goluseimas", 1.0, validadeMaisCinco);
	static Produto PastilhasMorango = new Produto("Pastilhas de Morango", "Goluseimas", 1.0, validadeMenosDez);
	static Produto Halls = new Produto("Halls", "Goluseimas", 1.0, validadeMenosDez);
	static Produto MMs = new Produto("M&M's", "Goluseimas", 1.0, validadeMaisCinco);
	static Produto[] goluseimas = {Sugus, PastilhasMenta, PastilhasMorango, Halls, MMs};
	
	static Produto Marmore = new Produto("Bolo Marmore", "Bolos", 0.80, validadeMaisCinco);
	static Produto Laranja = new Produto("Bolo de Laranja", "Bolos", 0.80, validadeMenosDez);
	static Produto Chocolate = new Produto("Bolo de Chocolate", "Bolos", 0.80, validadeMenosDez);
	static Produto Iogurte = new Produto("Bolo de Iogurte", "Bolos", 0.80, validadeMaisCinco);
	static Produto Maca = new Produto("Bolo de Maçã", "Bolos", 0.80, validadeMaisCinco);
	static Produto[] bolos = {Marmore, Laranja, Chocolate, Iogurte, Maca};
	
	static Produto[] random = {produtoRandom(aguas), produtoRandom(refrigerantes), produtoRandom(snacks), produtoRandom(goluseimas), produtoRandom(bolos)};
	
	public static void main(String[] args){
		
		maquinaVending = new MaquinaVending(5,10,3);
		preencheMaquinaProdutos();
		
		maquinaVending.debug();
		maquinaVending.menu();
	}
	
	public static Date adicionaValidade(GregorianCalendar g, int dias){
		g.add(GregorianCalendar.DATE, dias);
		Date data = g.getTime();
		return data;
	}
	
	public static Produto produtoRandom(Produto[] p){
		Random generator = new Random();
		return p[generator.nextInt(5)];
	}
	
	public static void preencheMaquinaProdutos(){
		for(int i=0;i<maquinaVending.getLinhas();i++){
			Produto p = produtoRandom(random); 
			for(int j=0;j<maquinaVending.getColunas();j++){
				maquinaVending.adicionaProduto(i, j, p);
			}
		}
	}
	
}